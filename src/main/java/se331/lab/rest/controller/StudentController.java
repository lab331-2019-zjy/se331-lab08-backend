package se331.lab.rest.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.entity.*;

import java.util.ArrayList;
import java.util.List;

@Controller
public class StudentController {
    List students;

    public StudentController() {
        this.students = new ArrayList<>();
        this.students.add(Student.builder()
                .id(1l)
                .studentId("592115508")
                .name("Junyu")
                .surname("Zhou")
                .gpa(100.0)
                .image("https://www.mobafire.com/images/avatars/teemo-classic.png")
                .penAmount(10)
                .description("Teemo is back!")
                .build());
    }

    @GetMapping("/students")
    public ResponseEntity getAllStudents() {
        return ResponseEntity.ok(this.students);
    }

    @GetMapping("/students/{id}")
    public ResponseEntity getStudentById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(this.students.get(Math.toIntExact(id - 1)));
    }

    @PostMapping("/students")
    public ResponseEntity saveStudent(@RequestBody Student student) {
        student.setId((long) this.students.size() + 1);
        this.students.add(student);
        return ResponseEntity.ok(student);
    }
}
